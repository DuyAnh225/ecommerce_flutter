import 'package:ecommerce_fl/constants/theme.dart';
import 'package:ecommerce_fl/screens/auth_ui/welcome/welcome.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Shop DA',
        theme: themeData,
        home: const Welcome());
  }
}
