import 'package:ecommerce_fl/constants/asset_images.dart';
import 'package:ecommerce_fl/widgets/primary_button/primary_button.dart';
import 'package:ecommerce_fl/widgets/top_titles/top_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Welcome extends StatelessWidget {
  const Welcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TopTitle(title: "Chào mừng", subTitle: "đến với cửa hàng DA"),
            Center(
                child: Image.asset(
              AssetImages.instance.welcomeImage,
              alignment: Alignment.center,
            )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CupertinoButton(
                  onPressed: () {},
                  padding: EdgeInsets.zero,
                  child: Icon(
                    Icons.facebook,
                    size: 35,
                    color: Colors.blue,
                  ),
                ),
                CupertinoButton(
                  onPressed: () {},
                  padding: EdgeInsets.zero,
                  child: Image.asset(
                    AssetImages.instance.googleLogo,
                    alignment: Alignment.center,
                    scale: 9.0,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 30.0,
            ),
            PrimaryButton(
              title: "Đăng nhập",
              onPressed: () {},
            ),
            const SizedBox(
              height: 18.0,
            ),
            PrimaryButton(
              title: "Đăng ký",
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
